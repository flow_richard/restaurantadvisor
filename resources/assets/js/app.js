'use strict';

/* App Module */

var restApp = angular.module('restApp', [
    'ngRoute',
    'restControllers',
    'ui.bootstrap',
    'uiGmapgoogle-maps'

]);

// restApp.config(function(uiGmapGoogleMapApiProvider) {
//     uiGmapGoogleMapApiProvider.configure({
//         key: 'AIzaSyC22oBhEnju9QnRiKAVYwRZKu9i0iZuCrQ',
//         v: '3.20', //defaults to latest 3.X anyhow
//         libraries: 'weather,geometry,visualization'
//     });
// })